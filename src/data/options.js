export const statusOptions = [
    { 
        key: 'Select',
        value: 'Select',
        text: 'Select',
    },
    { 
        key: 'Pending',
        value: 'Pending',
        text: 'Pending',
         icon: {
                    name:'clock outline',
                    color:'yellow'
                }
    },
    { 
        key: 'Paid',
        value: 'Paid',
        text: 'Paid',
         icon: {
                    name:'check',
                    color:'green'
                }
    },
    { 
        key: 'Overdue',
        value: 'Overdue',
        text: 'Overdue',
         icon: {
                    name:'clock outline',
                    color:'red'
                }
    }


];
