import React from 'react';
import { Header, Container, Statistic, List, Grid, Button } from 'semantic-ui-react';
import invoices from '../../data/invoices.json';
import Filter from './partials/filter';
import Invoice from './partials/invoice';

export default function Invoices(){
	return(
		<Container>
			<Header as="h1" content="Invoices"/>
			<Grid padded="horizontally" divided="vertically">
				<Grid.Row columns="2">
					<Grid.Column floated="left">
						<Statistic horizontal size="tiny" value={invoices.length} label="Total invoices" text  />
					</Grid.Column>
					<Grid.Column floated="right">
						<Filter />
						<Button floated="right" primary content="New Invoice" icon="add" />
					</Grid.Column>
				</Grid.Row>
				<Grid.Row>
					<Grid.Column>
						<List divided relaxed="very" >
							{invoices.map((item) => (
							<Invoice value={item}/>
							))}
						</List>
					</Grid.Column>
				</Grid.Row>
			</Grid>
		</Container>
	);
}
