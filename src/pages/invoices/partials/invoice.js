import React from 'react';
import { Button, Icon, List, Popup, Segment } from 'semantic-ui-react';

export default function Invoice(props){
	const state = props.value;

    function getIcon(status) {
		let res = 'check';
        if (status !== 'Paid') {
            res = 'clock outline';
        }
		return res;
    }
    function getColor(status) {
		let res = 'green';
        if (status !== 'Paid') {
            res = 'clock outline';
            if (status === 'Pending')
                res = 'yellow';
            else
				res = 'red';
        }
		return res;
    }
    return(
        <List.Item >
            <List.Content floated="right" verticalAlign="middle">
                <Button basic compact circular icon='angle down' size='mini'/>
            </List.Content>
			<List.Content floated="right" verticalAlign="middle">
				<Segment compact basic size="small">
					<Icon name="calendar alternate outline"/>
					Due on {state.due}<br/>
					<Icon name="dollar"/>
					{state.amount}
				</Segment>			
			</List.Content>
            <Popup
                content={state.status}
                inverted
                basic
                size="mini"
                mouseEnterDelay={500}
                mouseLeaveDelay={500}
                on="hover"
                trigger={<List.Icon name={getIcon(state.status)} color={getColor(state.status)} />}
                >
            </Popup>
            <List.Content verticalAlign="middle">
                <List.Header as="a" content={state.number} />
				<List.Description content={state.debtor.name} />
            </List.Content>
        
		</List.Item>

	);
}
