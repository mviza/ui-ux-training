import React from 'react';
import { Dropdown } from 'semantic-ui-react';
import { statusOptions } from '../../../data/options';

export default function Filter(props){

    return(
        <Dropdown
            options={statusOptions}
            placeholder="Filter by Status"
      />
	);
}
